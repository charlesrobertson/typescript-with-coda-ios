# TypeScript Demo: Coda IDE on iOS
---

This demo uses TypeScript 3x, using a javascript transpiler and Microsoft's transcription service. It was developed entirely on an iPhone 7, running iOS12. The code was written, using the Coda App IDE. Source control was provided by the Working Copy App.

## Installation Instructions

1. Create an 'index.html' page and add the following before the closing 'body' tag:

```
<script src="ts/[your_TypeScript_code].ts" charset="utf-8" type="text/TypeScript"></script>
		
<script src="transpilation/ts/TypeScriptServices.js" charset="utf-8"></script>

<script src="transpilation/ts/transpiler.js" charset="utf-8"></script>
```

2. Add all the required HTML above these script tags, including any semantic layout & design elements. 

3. Add any desired JS libraries or CSS links in the head of the document. 

## Demo Instructions

This demo is a simple 'todo' exercise. It emulates the part of a CMS, that might allow an administrator to add the personal details of students to a database.

The data stored by the application is displayed as 'JSON' text in the bottom panel. Four simple functions control the application:
					
1. Add student
2. Edit student
3. Remove student
4. Sort students

To edit a student, click on the student's name, in the students panel and then press the floppy disc icon, to save changes.

## Live Preview URL

<http://playground.application.me.uk/typescript-with-coda-ios/index.html>

## Demo Highlights | Generics

The TypeScript used in this project makes extensive use of generics like:

```typescript
interface iStudent < T, TUser > {
	addStudent(user: TUser): void;
	buildStudent(id: T, mode: string = "add"): void;
	editStudent(id: T): void;
	saveStudent(id: T): void;
	removeStudent(id: T): void;
}
```
Please view the demo for a more detailed explanation on TypeScript generics. 

# Drawbacks

Building TypeScript that uses a runtime transpiler & transcription requires that all classes, interfaces & other modules are added to a single script. The transcription service does not support the use of keywords, such as:

```
import
export
```
And the Coda IDE App does not transfer TypeScript syntax checking to the inbuilt console, so it is hard to tell whether certain TypeScript features are being implemented properly. Only javascript errors are picked up by the console after transpilation has been carried out. 

Although the Coda App is a great IDE for building TypeScript playground applications, it is probably not suitable for the creation of enterprise projects. 

## The Future

Hopefully, in the future, Panic will introduce a built in TypeScript transpiler that will support all the 3x language features, and a console that will support TypeScript lint & syntax checking. 

Support for building Angular projects, including a CLI terminal for both NodeJS & Angular CLI, would also be a very welcome addition for TypeScript programmers. And a fully functional Angular server for testing development projects would be an excellent addition to the Coda App's rich feature set. 

## The IDE

The Coda App does have very good FTP support for uploading files to a remote server, but, currently, has no inbuilt support for Git. However, it is possible to export entire directories to another great app, called Working Copy. This app provides superb support for Git commit, push & pull operations, and connects to all the major repository vendors, like:

1. BitBucket
2. GitHub
3. GitLab

The great thing about Working Copy is that, it can work out any changes that have been made in a Coda App project, after re-importing a directory. 

## The Cost

Although, the Coda App is GBP23.99 & the fully featured version of the Working Copy App is GBP15.99, this is actually great value for money, when you consider how much functionality these two apps provide. Having used several different IDE Apps, the Coda App, is by far the best in its class, especially when it is combined with the Working Copy App. It is a truly powerful combination. 





						
					