// class Utils

class Utils {

	constructor() {}

	public uuid < T > (type ? : string): T {
		const type = type || "string";
		let result = type == "string" ? "" : 0;
		for (var i = 0; i < 32; i++) {
			const random = Math.random() * 16 | 0;
			if (type == "string") {
				if (i === 8 || i === 12 || i === 16 || i === 20) {
					result += '-';
				}
			}
			if (type == "string") {
				result += (i === 12 ? 4 : (i === 16 ? (random & 3 | 8) : random))
					.toString(16);
			} else {
				result = this.generateRandomNumber(100000000, 999999999);
			}
		}
		return result;
	}

	private generateRandomNumber(min: number, max: number): number {
		const random_number = Math.random() * (max - min) + min;
		return Math.floor(random_number);
	}

	public log < T > (data: T): void {
		const consoleLogger = document.querySelector('#html-logger');
		if (typeof data === "object") {
			data = JSON.stringify(data, undefined, 4);
			if (data == "[]" || data == "{}") {
				data = "";
			}
		}
		consoleLogger.value = "";
		if (data != "") {
			consoleLogger.value = data + "\n";
		}
	}
	
	public readTextFile(file: string): void {
	    const rawFile = new XMLHttpRequest();
	    rawFile.open("GET", file, false);
	    rawFile.onreadystatechange = function () {
	        if(rawFile.readyState === 4) {
	            if(rawFile.status === 200 || rawFile.status == 0) {
	                let content = rawFile.responseText;
	                content = content.replace(/</g, "&lt;");
					$("#language-typescript").append(content);
					Prism.highlightAll();
	            }
	        }
	    }
	    rawFile.send(null);
	}
	
	public validationAlert(message: string): void {
		$("#validation-alert").append(message);
		$("#validation-alert").fadeIn().delay(3000).fadeOut();
	}
	
	public sleep(milliseconds: number): void {
	  const start = new Date().getTime();
	  for (var i = 0; i < 1e7; i++) {
	    if ((parseInt(new Date().getTime() - start)) > milliseconds){
	        break;
	    }
	  }
	}

}

// class User

class User {

	userid: number;
	email: string;
	forename: string;
	surname: string;
	position: number;
	createdAt: string;
	updatedAt: string;

	constructor(obj ? : any) {
		this.userid = obj && obj.userid || 0;
		this.email = obj && obj.email || "";
		this.forename = obj && obj.forename || "";
		this.surname = obj && obj.surname || "";
		this.position = obj && obj.position || 0;
		this.createdAt = obj && obj.createdAt || new Date();
		this.updatedAt = obj && obj.updatedAt || new Date();
	}

}

// class Student

interface iStudent < T, TUser > {
	addStudent(user: TUser): void;
	buildStudent(id: T, mode: string = "add"): void;
	editStudent(id: T): void;
	saveStudent(id: T): void;
	removeStudent(id: T): void;
	up(id1: T, id2: T, ids: any): void;
	down(id1: T, id2: T, ids: any): void;
}

interface iUserStudent {
	email: string;
	forename: string;
	surname: string;
}

class Student implements iStudent < T, TUser > {

	students: array = [];
	utils: Utils = new Utils();
	direction: string = "asc";
	height: number = 56;
	margin: number = 10;
	duration1: number = 0.5;
	duration2: number = this.duration1 + 0.1;
	ease: any = Elastic.easeOut.config(1, 0.3);
	//ease: any = Linear.easeNone;

	constructor() {
		const addEl = document.querySelector("#add-student");
		addEl.addEventListener("click", this.buildStudent.bind(this), false);
		const listSortAnimationEl = document.querySelector("#list-sort-animation");
	listSortAnimationEl.addEventListener("change", this.changeListSortAnimation.bind(this), false);
		const sortEl = document.querySelector("#sort-students");
		sortEl.addEventListener("click", this.sortStudent.bind(this), false);
		const toggle1El = document.querySelector("footer > div > h2#demo-instruction-header > i");
		toggle1El.addEventListener("click", this.toggleInstruction.bind(this), false);
		const toggle2El = document.querySelector("footer > div > h2#typescript-header > i");
		toggle2El.addEventListener("click", this.toggleTypescript.bind(this), false);
        const toggle3El = document.querySelector("footer > div > h2#demo-highlight-header > i");        toggle3El.addEventListener("click", this.toggleHighlight.bind(this), false);
        this.utils.readTextFile("ts/student.ts");
	}

	private buildStudent < T > (id: T, mode: string = "add", obj: any = {}): void {
		const id = isNaN(id) ? id : parseInt(id);
		let email = document.querySelector("#email");
		if('email' in obj) {
			email = obj['email'];
			const emailVal =  email.toLowerCase();
		}
		else{
			const emailVal = email ? email.value.toLowerCase() : "";
		}
		
		let forename = document.querySelector("#forename");
		if('forename' in obj) {
			forename = obj['forename'];
			const forenameVal = forename.toLowerCase();
		}
		else{
			const forenameVal = forename ? forename.value.toLowerCase() : "";
		}
		
		let surname = document.querySelector("#surname");
		if('surname' in obj) {
			surname = obj['surname'];
			const surnameVal = surname.toLowerCase();
		}
		else{
			const surnameVal = surname ? surname.value.toLowerCase() : "";
		}
		if (emailVal != "" && emailVal.indexOf("@") != -1 && forenameVal != "" && surnameVal != "") {
			forename = forenameVal.charAt(0).toUpperCase() + forenameVal.substr(1);
			surname = surnameVal.charAt(0).toUpperCase() + surnameVal.substr(1);
			if (mode == "add") {
				const user = {
					email: emailVal,
					forename: forename,
					surname: surname
				}
				this.addStudent(user);
			}
			else{
				const student = this.students.filter((student) => {
					const comparator = typeof id == 'number' ? student['userid'] === id : student['userid'].toLowerCase() === id.toLowerCase();
					return comparator;
				});
				if ($.isArray(student) && student.length == 1) {
					const _student = student[0];
					const _students = this.students.filter((student) => {
						const comparator = typeof id == 'number' ? student['email'].toLowerCase() === emailVal.toLowerCase() && student['userid'] !== id : student['email'].toLowerCase() === emailVal.toLowerCase() && student['userid'].toLowerCase() !== id.toLowerCase();
						return comparator;
					});
					const duplicateEmail = $.isArray(_students) && _students.length > 0 ? true : false;
					if (!duplicateEmail) {
						_student['email'] = emailVal;
						_student['forename'] = forename;
						_student['surname'] = surname;
						_student['updatedAt'] = new Date();
						this.outputStudents();
					}
					else{
						this.utils.validationAlert("A user with this e-mail address already exists...");
					}
				}
			}
		}
	}

	private addStudent < TUser extends iUserStudent > (user: TUser): void {
		const idtype = $("input[type='radio']:checked").val();
		const userid = idtype == "string" ? this.utils.uuid < string > ("string") : this.utils.uuid < number > ("number");
		let _user = {
			userid: userid
		};
		const student = Object.assign(_user, user);
		const _student = new User(student);
		const _students = this.students.filter((student) => {
			return student['email'].toLowerCase() === _student['email'].toLowerCase();
		});
		if ($.isArray(_students) && _students.length == 0) {
			this.students.push(_student);
		}
		this.outputStudents();
	}
	
	private toggleIdType(): void {
		if (this.students.length > 0) {
			const hasDescendant = $("#id-type").has("strong").length ? true : false;
			if (!hasDescendant) {
				const idtypestring = isNaN(this.students[0]['userid']) ? '<strong>ID Type:</strong><span> UUID</span>' : '<strong>ID Type:</strong><span> numeric</span>';
	    		$("#id-types").css({'display':'none'});
	    		$("#id-type").html(idtypestring).css({'display':'block'});
    		}
		}
		else{
			$("#id-types").css({'display':'block'});
	    	$("#id-type").html("").css({'display':'none'});
		}
	}

	private outputStudents(): void {
		this.toggleIdType();
		this.sortStudents();
		this.displayStudents();
		this.logStudents();
	}

	private sortStudent(): void {
		this.sortStudents(true);
		this.displayStudents();
		this.logStudents();
	}
	
	private logStudents(): void{
		const displayUpdatedDate = $("input[type='checkbox']:checked").val() ?true : false;
		const students = this.students.map( (student) => {
			if (!displayUpdatedDate) {
				delete student['updatedAt'];
			}
			return student;
		})
		this.utils.log < any > (students);
	}

	private sortStudents(toggle: boolean = false, direction: string = "asc"): void {
		if (toggle) {
			var direction = this.direction == "asc" ? "desc" : "asc";
		}
		this.direction = direction;
		this.students.sort(function (a, b) {
			const _a = a.surname.toLowerCase();
			const _b = b.surname.toLowerCase();
			if (direction == "asc") {
				if (_a < _b) {
					return -1;
				}
				if (_a > _b) {
					return 1;
				}
			} else {
				if (_a < _b) {
					return 1;
				}
				if (_a > _b) {
					return -1;
				}
			}
			return 0;
		});
	}

	private displayStudents(): void {
		$("#students").html('');
		let height = this.height;
		let margin = this.margin;
		let top = 0;
		var that = this;
		$.each(this.students, function (index, obj) {
			let previous =  null;
			let next = null;
			let previousids = '1-1';
			let nextids = '1-1';
			if(index !== 0) {
				previous = that.students[index-1]['userid'];
			}
			else{
				previousids = '1-0';
				previous = that.students[that.students.length-1]['userid'];
			}
			if(index !== (that.students.length - 1)) {
				next = that.students[index+1]['userid'];
			}
			else{
				nextids = '1-0';
				next = that.students[0]['userid'];
			}
			//console.log('ids: ',ids);
			let _height = height + (10 * (index + 1));
			$("#students").css({'height':_height + 'px'});
			$("#students").append('<p class="student" id="' + obj['userid'] + '" data-role-userid="' + obj['userid'] + '" style="top:' + top + 'px;"><span data-role-userid="' + obj['userid'] + '" onclick="editStudent(\'' + obj['userid'] + '\')">' + obj['surname'] + ', ' + obj['forename'] + '</span><i class="fa fa-arrow-circle-up up" data-role-userid="' + obj['userid'] + '" onclick="up(\'' + obj['userid'] + '\',\'' + previous + '\',\'' + previousids + '\')"></i><i class="fa fa-arrow-circle-down down" data-roles-userid="' + obj['userid'] + '" onclick="down(\'' + obj['userid'] + '\',\'' + next + '\',\'' + nextids + '\')"></i><i class="fa fa-floppy-o edit-student" data-role-userid="' + obj['userid'] + '" onclick="saveStudent(\'' + obj['userid'] + '\')"></i><i class="fa fa-minus-circle remove-student" data-role-userid="' + obj['userid'] + '" onclick="removeStudent(\'' + obj['userid'] + '\')"></i></p>');
			that.students[index]['position'] = parseInt(top);
			top = height + (margin * (index + 1));
			height = height + that.height;
		});
	}
	
	public up < T > (id1: T, id2: T, ids: any): void {
		const id1 = isNaN(id1) ? id1 : parseInt(id1);
		const id2 = isNaN(id2) ? id2 : parseInt(id2);
		const ids = ids.length == 2 ? ids : [1,1];
		//console.log("ids ",ids);
		let moveOthers = false;
		if(ids[1] === '0') {
			moveOthers = true;
		}
		const current = document.querySelector("p[data-role-userid='" + id1 + "']");
		let previous = null;
		if(id2) {
			previous = document.querySelector("p[data-role-userid='" + id2 + "']");
		}
		if(current && previous){
			const cTop = $(current).css('top');
			const pTop = $(previous).css('top');
			this.students.map( (student) => {
				if(student['userid'] === id1 && ids[0] === '1') {
					student['position'] = parseInt(pTop);
				}
				if(student['userid'] === id2 && ids[1] === '1') {
					student['position'] = parseInt(cTop);
				}
			});
			var that = this;
			if(ids[0] === '1') {
		        TweenMax.to(current,this.duration2,{
		          top:pTop,
		          ease: this.ease,
		          onComplete:function(){
	            			that.reOrderStudents();
	            	that.displayStudents();
	            	that.logStudents();
	          	  }
		        });
	        }
	        if(ids[1] === '1') {
		        TweenMax.to(previous,this.duration1,{
		          top:cTop,
		          ease: this.ease
		        });
	        }
	        if(moveOthers) {
	    	this.students.filter( (student) => {
	        		return student['userid'].toLowerCase() !== id1.toLowerCase();
	        	}).map( (student, index) => {
	        		let top = student['position'] - (this.height + this.margin);
	        		student['position'] = top > 0 ? top : 0;
		        	TweenMax.to('p#' + student['userid'],this.duration1,{
			          top:student['position'] + 'px'
			        });
		        })
	        }
		}
	}
	
	public down < T > (id1: T, id2: T, ids: any): void {
		const id1 = isNaN(id1) ? id1 : parseInt(id1);
		const id2 = isNaN(id2) ? id2 : parseInt(id2);
		const ids = ids.length == 2 ? ids : [1,1];
		//console.log("ids ",ids);
		let moveOthers = false;
		if(ids[1] === '0') {
			moveOthers = true;
		}
		const current = document.querySelector("p[data-role-userid='" + id1 + "']");
		let next = null;
		if(id2) {
			next = document.querySelector("p[data-role-userid='" + id2 + "']");
		}
		if(current && next){
			const cTop = $(current).css('top');
			const nTop = $(next).css('top');
			this.students.map( (student) => {
				if(student['userid'] === id1 && ids[0] === '1') {
					student['position'] = parseInt(nTop);
				}
				if(student['userid'] === id2 && ids[1] === '1') {
					student['position'] = parseInt(cTop);
				}
			});
			var that = this;
			if(ids[0] === '1') {
		        TweenMax.to(current,this.duration2,{
		          top:nTop,
		          ease: this.ease,
		          onComplete:function(){
	            	that.reOrderStudents();
	            	that.displayStudents();
	            	that.logStudents();
	          	  }
		        });
	        }
	        if(ids[1] === '1') {
		        TweenMax.to(next,this.duration1,{
		          top:cTop,
		          ease: this.ease
		        });
	        }
	        if(moveOthers) {
	    	this.students.filter( (student) => {
	        		return student['userid'].toLowerCase() !== id1.toLowerCase();
	        	}).map( (student, index) => {
	        		let top = student['position'] + (this.height + this.margin);
	        		student['position'] = top > 0 ? top : 0;
		        	TweenMax.to('p#' + student['userid'],this.duration1,{
			          top:student['position'] + 'px'
			        });
		        })
	        }
		}
	}
	
	private reOrderStudents(): void {
		this.students.sort(function (a, b) {
			return a.position - b.position;
		});
	}

	public editStudent < T > (id: T): void {
		const id = isNaN(id) ? id : parseInt(id);
		const student = this.students.filter((student) => {
			const comparator = typeof id == 'number' ? student['userid'] === id : student['userid'].toLowerCase() === id.toLowerCase();
			return comparator;
		});
		if ($.isArray(student) && student.length == 1) {
			$("#email").val(student[0]['email']);
			$("#forename").val(student[0]['forename']);
			$("#surname").val(student[0]['surname']);
		}
		
	}
	
	public saveStudent < T > (id: T): void {
		const id = isNaN(id) ? id : parseInt(id);
		this.buildStudent<string>(id, "edit");
	}
	
	public removeStudent < T > (id: T): void {
		const id = isNaN(id) ? id : parseInt(id);
		const students = this.students.filter((student) => {
			const comparator = typeof id == 'number' ? student['userid'] !== id : student['userid'].toLowerCase() !== id.toLowerCase();
			return comparator;
		});
		this.students = students;
		this.outputStudents();
	}
	
	private changeListSortAnimation(event): void {
		console.log(event.target.value);
		const val = event.target.value;
		switch (val) {
			case 'elastic':
				this.ease = Elastic.easeOut.config(1, 0.3);
				break;
			case 'linear':
				this.ease = Linear.easeNone;
				break;
			default:
				this.ease = Elastic.easeOut.config(1, 0.3);
   		}
	}

	private toggleInstruction(): void {
		$("footer > div > article#demo-instruction").fadeToggle("slow", "linear");
	}

	private toggleTypescript(): void {
		$("footer > div > article#typescript").fadeToggle("slow", "linear");
	}
	
	private toggleHighlight(): void {
		$("footer > div > article#demo-highlight").fadeToggle("slow", "linear");
	}

}

// global namespace

var student = new Student();

const array = [{email:'m.mouse@disney.com',forename:'mickey',surname:'mouse'},{email:'d.duck@disney.com',forename:'donald',surname:'duck'},{email:'p.dog@disney.com',forename:'pluto',surname:'dog'},{email:'y.bear@disney.com',forename:'yogi',surname:'bear'},{email:'mn.mouse@disney.com',forename:'minnie',surname:'mouse'},{email:'df.duck@disney.com',forename:'daffy',surname:'duck'}];
array.map( (obj) => {
	student.buildStudent(null,'add',obj);
});

function up < T > (id1: T, id2: T, ids: any): void {
	const id1 = isNaN(id1) ? id1 : parseInt(id1);
	const id2 = isNaN(id2) ? id2 : parseInt(id2);
	const ids = ids ? ids.split("-") : [1,1];
	student.up < string | null > (id1,id2,ids);
}

function down < T > (id1: T, id2: T, ids: any): void {
	const id1 = isNaN(id1) ? id1 : parseInt(id1);
	const id2 = isNaN(id2) ? id2 : parseInt(id2);
	const ids = ids ? ids.split("-") : [1,1];
	student.down < string | null > (id1,id2,ids);
}

function editStudent < T > (id: T): void {
	const id = isNaN(id) ? id : parseInt(id);
	student.editStudent < string > (id);
}

function saveStudent < T > (id: T): void {
	const id = isNaN(id) ? id : parseInt(id);
	student.saveStudent < string > (id);
}

function removeStudent < T > (id: T): void {
	const id = isNaN(id) ? id : parseInt(id);
	student.removeStudent < string > (id);
}