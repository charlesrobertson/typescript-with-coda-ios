
// class Utils

class Utils {

	constructor() {}

	private generateRandomNumber(min: number, max: number): number {
		const random_number = Math.random() * (max - min) + min;
		return Math.floor(random_number);
	}

	public log < T > (data: T): void {
		const consoleLogger = document.querySelector('#html-logger');
		if (typeof data === "object") {
			data = JSON.stringify(data, undefined, 4);
			if (data == "[]" || data == "{}") {
				data = "";
			}
		}
		consoleLogger.value = "";
		if (data != "") {
			consoleLogger.value = data + "\n";
		}
	}
	
}

// class Test

class Test {

	comments: any = [];
	utils: Utils = new Utils();

	constructor() {
		
		var commentid = 1;
		
		for (var i = 0; i < 5; i++) {
			const obj = {};
			const date = new Date(2012, 1, i + 1);
			obj['commentid'] = commentid;
			obj['userid'] = 1;
			obj['replyToCommentid'] = commentid;
			obj['createdAt'] = date;
			let dateFormat = moment(date).format('ddd, MMM DD, YYYY');
			obj['shortDate'] = dateFormat;
			obj['replies'] = [];
    		this.comments.push(obj);
    		commentid++;
		}
		
		for (var i = 0; i < 5; i++) {
			const obj = {};
			const date = new Date(2012, 2, i + 1);
			obj['commentid'] = commentid;
			obj['userid'] = 2;
			obj['replyToCommentid'] = commentid;
			obj['createdAt'] = date;
			let dateFormat = moment(date).format('ddd, MMM DD, YYYY');
			obj['shortDate'] = dateFormat;
			obj['replies'] = [];
    		this.comments.push(obj);
    		commentid++;
		}
		
		for (var i = 0; i < 5; i++) {
			const obj = {};
			const date = new Date(2012, i + 3, i + 1);
			obj['commentid'] = commentid;
			obj['userid'] = 3;
			obj['replyToCommentid'] = this.utils.generateRandomNumber(1,10);
			obj['createdAt'] = date;
			let dateFormat = moment(date).format('ddd, MMM DD, YYYY');
			obj['shortDate'] = dateFormat;
			obj['replies'] = [];
    		this.comments.push(obj);
    		commentid++;
		}
		
// 		this.sortComments();
		
		this.sortCommentsWithReplies('desc');
		
		this.comments.sort(
		    firstBy('replyToCommentid',-1)
		    .thenBy('commentid')
		);
		
		this.utils.log < any > (this.comments);
		
// 		this.utils.log < any > (this.comments);
		
	}
	
	sortCommentsWithReplies(sortMethod: string = 'desc'): void {
		switch (sortMethod) {
			case 'desc':
    
				var comments1 = [];
				comments1 = this.comments.filter( (comment) => {
					return comment['commentid'] === comment['replyToCommentid'];
				});
				var comments2 = [];
				comments2 = this.comments.filter( (comment) => {
					return comment['commentid'] !== comment['replyToCommentid'];
				});
				comments1.map( (comment1) => {
					comments2.map( (comment2) => {
						if (comment1['commentid'] === comment1['replyToCommentid'] && comment2['replyToCommentid'] === comment1['commentid']) {
						comment1['replies'].push(comment2);
						}
					});
				});
				var comments = [];
				comments1.map( (comment) => {
					comments.push(comment);
					if(comment['replies'].length) {
						comment['replies'].map( (item) => {
							comments.push(item);
						});
					}
					comment['replies'] = [];
				});
				this.comments = comments;
				break;
			case 'asc':
				this.comments.sort(
				    firstBy('replyToCommentid',-1)
				    .thenBy('commentid')
				);
				break;
			default:
		}
	}
	
	randomDate(start: any, end: any): any {
	    return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
	}
	
	getRandomArbitrary(min: any, max:any): any {
	    return Math.random() * (max - min) + min;
	}

	sortComments(): void {
	  this.comments.sort(function(a, b) {
	    const dateA: any = new Date(a.createdAt), dateB: any = new Date(b.createdAt);
	    return dateB - dateA;
	  });
	}

}

var test = new Test();


