<!DOCTYPE html>
<html>
	<head>
		<title>TypeScript Demo: Coda IDE on iOS</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="TypeScript Demo: Coda IDE on iOS" />
		<meta name="robots" content="index, follow" />
		<meta name="twitter:card" content="summary" />
		<meta name="twitter:site" content="@charlesr1971" />
		<meta name="twitter:creator" content="@charlesr1971" />
		<meta property="og:url" content="http://playground.application.me.uk/typescript-with-coda-ios/index.html" />
		<meta property="og:title" content="TypeScript with Coda iOS" />
		<meta property="og:description" content="This demo uses TypeScript 3x, using a JavaScript transpiler and Microsoft's transcription service. It was developed entirely on an iPhone 7, running iOS12. The code was written, using the Coda App IDE. Source control was provided by the Working Copy App." />
		<meta property="og:image" content="http://playground.application.me.uk/typescript-with-coda-ios/images/typescript-logo.png" />

		<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		
		<script src="https://code.jquery.com/jquery-3.3.1.min.js"   integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="   crossorigin="anonymous"></script>
		
		<script src="js/TweenMax.min.js" data-manual></script>
		
		<link rel="stylesheet" href="css/prism.css" data-noprefix />
		
	</head>
	<body>
		
		<nav>
			<h1><strong>TypeScript Demo </strong><span style="font-weight:normal;color:rgba(255,255,255,0.6);">| Coda IDE on iOS</span></h1>
		</nav>
		
		<div class="header-image">
		</div>
		
		<footer>
			<div>
				<h2 class="section-header">Language <span style="font-weight:normal;color:rgba(255,255,255,0.6);">| TypeScript</span><img src="images/typescript-square-logo.jpg" /></h2>
				<section class="has-header">
					This demo uses TypeScript 3x, a JavaScript transpiler and Microsoft's transcription service. It was developed on an iPhone 7, running iOS12. The code was written, using the Coda App IDE. Source control was provided by the Working Copy App.<br /><br />
					<div class="image-container">
						<a href="https://geo.itunes.apple.com/us/app/coda/id500906297?mt=8&at=11l4BV" target="itunes" class="logo"><img src="images/coda-app-logo.jpg" /></a>
						<a href="https://itunes.apple.com/us/app/working-copy/id896694807?mt=8&amp;uo=6&amp;at=1000lHq&amp;ct=workingcopyapp" target="itunes" class="logo"><img src="images/working-copy-logo.jpg" /></a>
					</div>
				</section>
				<h2 id="typescript-header" class="article-header">
					TypeScript<i class="fa fa-plus"></i>
				</h2>
				<article id="typescript" style="display:none;">
					<pre class="line-numbers">
<code class="language-typescript" id="language-typescript"><img src="images/loading.gif" /></code>
					</pre>
				</article>
				<h2 id="demo-highlight-header" class="article-header" style="margin-top:20px;">
					Demo Highlight <span style="font-weight:normal;color:rgba(255,255,255,0.6);">| Generics</span><i class="fa fa-plus"></i>
				</h2>
				<article id="demo-highlight" style="display:none;">
					In TypeScript, one of the main tools in the toolbox for creating reusable components is generics, that is, being able to create a component that can work over a variety of types rather than a single one. This allows users to consume these components and use their own types.<br /><br />The demo allows an administrator to choose whether the application should create a UUID [string] or a numeric ID. One way to notify TypeScript of the correct type, would be to use 'any'. While using 'any' is certainly generic in that it will cause the function to accept any and all types for the type of arg, we actually are losing the information about what that type was, when the function returns. If we passed in a number, the only information we have is that any type could be returned.<br /><br /> Instead, we need a way of capturing the type of the argument, in such a way that we could also use it to denote what is being returned. In the following function, the return type is actually 'void', but we could have added a generic return type. Here, we will use a type variable, a special kind of variable that works on types rather than values.<pre class="line-numbers" ><code class="language-typescript" style="margin:0px;padding:0px;">
	public editStudent &lt; T &gt; (id: T): void {
	...
	}
</code>
</pre>
We've now added a type variable T to the identity function. This T allows us to capture the type the user provides (e.g. number or string), so that we can use that information later.<br /><br />We say that this version of the identity function is generic, as it works over a range of types. Unlike using any, it's also just as precise (ie, it doesn't lose any information) as an identity function that uses numbers for the argument.<br /><br />Once we've written the generic identity function, we can call it in one of two ways. The first way is to pass all of the arguments, including the type argument, to the function:<pre class="line-numbers">
<code class="language-typescript" style="margin:0px;padding:0px;">
	let id = editStudent < string > ("83580feb-ac03-4e48-87a7-9919d2a4f96b");
	let id = editStudent < number > (134160629);
</code>
</pre>
Here we explicitly set T to be string as one of the arguments to the function call, denoted using the <> around the arguments rather than ().<br /><br />The second way is also perhaps the most common. Here we use type argument inference - that is, we want the compiler to set the value of T for us automatically based on the type of the argument we pass in:<pre class="line-numbers">
<code class="language-typescript" style="margin:0px;padding:0px;">
	let id = editStudent("83580feb-ac03-4e48-87a7-9919d2a4f96b");
	let id = editStudent(134160629);
</code>
</pre>
Notice that we didn't have to explicitly pass the type in the angle brackets (<>); the compiler just looked at the value passed in, and set T to its type. While type argument inference can be a helpful tool to keep code shorter and more readable, you may need to explicitly pass in the type arguments as we did in the previous example when the compiler fails to infer the type, as may happen in more complex examples.


				</article>
				<h2 id="demo-instruction-header" class="article-header" style="margin-top:20px;">
					Demo Instructions<i class="fa fa-plus"></i>
				</h2>
				<article id="demo-instruction" style="display:none;">
					This demo is a simple 'todo' exercise. It emulates the part of a CMS, that might allow an administrator to add the personal details of students to a database.<br /><br />The data stored by the application is displayed as 'JSON' text in the bottom panel. Four simple functions control the application:
					<ol>
						<li>
							Add student
						</li>
						<li>
							Edit student
						</li>
						<li>
							Remove student
						</li>
						<li>
							Sort students
						</li>
					</ol>
					To edit a student, click on the student's name, in the students panel and then press the floppy disc icon, to save changes. 
				</article>
				<fieldset>
					<div id="id-types">
						<label class="container-radio">Create UUID
						<input type="radio" name="idTypeRadio" value="string" checked="checked">
						<span class="checkmark-radio"></span>
						</label>
						<label class="container-radio">Create numeric ID
							<input type="radio" name="idTypeRadio" value="number">
							<span class="checkmark-radio"></span>
						</label>
					</div>
					<div id="id-type" style="display:none;">
						<!-- ID Type: -->
					</div>
					<input class="field" type="text" id="email" placeholder="E-mail" autocomplete="on">
					<div id="validation-alert" style="display:none;"></div>
					<input class="field" type="text" id="forename" placeholder="Forename" autocomplete="on">
					<input class="field" type="text" id="surname" placeholder="Surname" autocomplete="on">
					<label class="container-checkbox">Record Update Date
						<input type="checkbox" name="recordUpdateDateCheckbox">
						<span class="checkmark-checkbox"></span>
					</label>
					<select id="list-sort-animation" class="select-css">
						<option value="">List Sort Animation Type</option>
						<option value="elastic">Elastic</option>
						<option value="linear">Linear</option>
					</select>
					<div class="button-container">
						<button class="btn btn-primary" type="submit" id="add-student">Add Student</button>
						<button class="btn btn-warning" type="submit" id="sort-students">Sort Students</button>
					</div>
				</fieldset>
				<div id="students">
				</div>
				<textarea class="field" id="html-logger"></textarea>
				<div class="avatar-container">
					<img src="images/avatar.jpg" />
					<h3>About the Author</h3>
					
					Charles Robertson is a technical developer, with over 20 years experience, specialising in Coldfusion, JavaScript, TypeScript, Angular, Java & Objective-c
					<h3 style="margin-top:20px;">About this Article</h3>
					<table>
						<tr>
							<td>Published </td>
							<td>20th Nov, 2018</td>
						</tr>
						<tr>
							<td>Git Repo</td>
							<td><a href="https://bitbucket.org/charlesrobertson/typescript-with-coda-ios/src/master/README.md" target="_blank">BitBucket</a></td>
						</tr>
						<tr>
							<td>Contact</td>
							<td><a href="mailto:cdesign@btinternet.com">E-mail</a></td>
						</tr>
					</table>
				</div>
				
				<!-- START: Disqus Installation -->
					
				<div id="disqus_thread"></div>
				<script>
					var disqus_config = function () {
					this.page.url = "http://playground.application.me.uk/typescript-with-coda-ios/index.html";
					this.page.identifier = "aHR0cDovL3BsYXlncm91bmQuYXBwbGljYXRpb24ubWUudWsvdHlwZXNjcmlwdC13aXRoLWNvZGEtaW9zL2luZGV4Lmh0bWw=";
					};
					(function() {
						var d = document, s = d.createElement('script');
						s.src = 'https://playground-3.disqus.com/embed.js';
						s.setAttribute('data-timestamp', +new Date());
						(d.head || d.body).appendChild(s);
					})();
				</script>
				<noscript>
					Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a>
				</noscript>
				
				<!-- END: Disqus Installation -->
				
				<div style="height:50px;">
				</div>
				
			</div>
		</footer>
		
		<script src="ts/student.ts" charset="utf-8" type="text/typescript"></script>
		
		<script src="transpilation/ts/typescriptServices.js" charset="utf-8"></script>
		<script src="transpilation/ts/transpiler.js" charset="utf-8"></script>
		
		<script src="js/prism.js" data-manual></script>
		
	</body>
</html>